# Images Docker

![https://gitlab.com/tonyschmitt/indus/image-docker/-/commits/master](https://gitlab.com/tonyschmitt/indus/image-docker/badges/master/pipeline.svg?key_text=Build&key_width=60)

## 🧐 A propos

Ce projet permet de build plusieurs images docker de base utilisée sur d'autres projets.

## 📑 Liste des images

- [php](php/php.md)
