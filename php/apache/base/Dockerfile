# -------------------------------------------------- Build Arguments ---------------------------------------------------
ARG CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX
ARG PHP_VERSION="8.0"
ARG COMPOSER_VERSION="2.1"
ARG APCU_VERSION="5.1.20"
ARG IMAGE_DEPS=""
ARG USER_ID=1000
ARG GROUP_ID=1000

# --------------------------------------------------- Image Composer ---------------------------------------------------

FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/composer:${COMPOSER_VERSION} as composer

# ======================================================================================================================
#                                                      --- Base ---
# --------------- Installe l'image de base php avec toutes les extensions et configurations nécessaires. ---------------
# ======================================================================================================================

FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/php:${PHP_VERSION}-apache as base

# Build Arguments

ARG IMAGE_DEPS

# --------------------------------------------- Mets à jour les dépendances --------------------------------------------

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get upgrade -y --no-install-recommends \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# ------------------------------------------ Installe les packages nécessaires -----------------------------------------

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends wget libicu-dev libzip-dev zip unzip ${IMAGE_DEPS} \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# --------------------------------------------- Installe les extensions PHP --------------------------------------------

ARG APCU_VERSION

RUN docker-php-ext-configure zip && docker-php-ext-install opcache intl zip \
    && a2enmod rewrite \
    && pecl install apcu \
    && docker-php-ext-enable apcu


# ---------------------------------------------------- Permissions -----------------------------------------------------

ARG USER_ID
ARG GROUP_ID

# - Configure l'utilisateur 1000 pour la compatibilité avec docker
RUN userdel -f www-data \
    && if getent group www-data ; then groupdel www-data; fi \
    && groupadd -g ${GROUP_ID} www-data \
    && useradd -l -u ${USER_ID} -g www-data www-data \
    && usermod -aG sudo www-data \
    && install -d -m 0755 -o www-data -g www-data /home/www-data \
    && chown --changes --silent --no-dereference --recursive \
          --from=33:33 ${USER_ID}:${GROUP_ID} \
        /home/www-data \
        /var/www

# --------------------------------------------------- Configuration ----------------------------------------------------

# Timezone
ENV TZ=Europe/Paris
RUN echo "date.timezone = Europe/Paris" > /usr/local/etc/php/conf.d/timezone.ini \
    && mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# Ajout de la configuration de base PHP
COPY conf/php.ini $PHP_INI_DIR/conf.d/app.ini

# Ajout de la configuration de PHP-APACHE
COPY errors /errors
COPY conf/vhost.conf /etc/apache2/sites-available/000-default.conf
COPY conf/apache-prod.conf /etc/apache2/conf-available/z-app.conf

RUN a2enconf z-app

# ------------------------------------------------------ Composer ------------------------------------------------------

RUN mkdir -p /var/www/.composer && chown -R www-data:www-data /var/www/.composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# -------------------------------------------------------- APP ---------------------------------------------------------

RUN mkdir /app && chown -R www-data:www-data /app
WORKDIR /app
